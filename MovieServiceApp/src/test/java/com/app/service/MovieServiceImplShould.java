package com.app.service;

import com.app.repo.MovieRepo;
import com.app.service.api.MovieService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class MovieServiceImplShould {

    @InjectMocks
    private MovieServiceImpl movieService;

    @Mock
    private MovieRepo movieRepo;

    @Mock
    private MongoTemplate mongoTemplate;


    @Test
    public void sampleTest() {
        assertThat(true).isTrue();

        assertThat(movieService.equals(null)).isFalse();
    }

    @Test
    public void test2() {
        String actual = movieService.justForFun("Bhargo");
        assertThat(actual).isEqualTo("Hello Bhargo");
    }
}
